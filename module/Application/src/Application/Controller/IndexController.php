<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Employee;
use Application\Entity\Inputs;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;

    /**
     * Método para obtener el servicio de
     * Doctrine dentro del Controlador
    */
    private function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }
    
    public function __constructor() {}
    public function setServiceLocator($serviceLocator) 
    {
         $this->serviceLocator = $serviceLocator;
    }
    
    /**
     * 
     * @return ViewModel
     */
    public function indexAction()
    {
        $em = $this->getEntityManager();
        $employees =  $em->getRepository('\Application\Entity\Employee')->getAll();
        $employeeForm = $this->getServiceLocator()->get('EmployeeForm');
        
        return new ViewModel(array('employees'=>$employees,"form"=>$employeeForm));
    }
    
    
    public function addAction(){
        $request = $this->getRequest();
        $em = $this->getEntityManager();
        $flag = false;
        $message = '';
        
         $data  =  array('status'=>200);
        
        
            $form = $this->getServiceLocator()->get('EmployeeForm');
            $form->setData($request->getPost());
            
           
            //$e = new \Application\Form\EmployeeForm();
            if(!is_float($form->get('inputYears'))){
                $form->getInputFilter()->get('inputYears')->setErrorMessage('mal',  \Zend\Validator\NotEmpty::IS_EMPTY);
            }
            if($form->isValid()){
                try{
                    $em->getConnection()->beginTransaction();
                    $employee =  new Employee();
                    $employee->setFirstname($this->params()->fromPost('firstname'));
                    $employee->setLastname($this->params()->fromPost('lastname'));
                    $employee->setName($this->params()->fromPost('name'));
                    $employeInputs = new Inputs();
                    $employeInputs->setBirthdate(new \DateTime(date('Y-m-d', strtotime($this->params()->fromPost('birthdate')))));
                    $employeInputs->setAnualInput($this->params()->fromPost('inputYears'));
                    $employeInputs->setIdEmployee($employee);
                    $em->persist($employee);
                    $em->persist($employeInputs);
                    
                    $em->flush();
                    $em->getConnection()->commit();
                    $flag =true;
                    $message = 'El empleado se ha agregado correctamente';

                } catch(\Exception $exce){
                    $message = 'Hubo problemas al intentar agregar el empleado';
                    $em->getConnection()->rollBack();
                    $data['status'] = 500 ;
                }
            } else {
                if($request->isXmlHttpRequest()){
                    $rendered = $this->getServiceLocator()->get('ViewRenderer');
                    $viewModelo  = new ViewModel(array('form' => $form));
                    $viewModelo->setTemplate('application/index/partial/formEmpoyee.phtml');
                    $data['content']  = $rendered->render($viewModelo);
                } else {
                     $data['content'] = $form->getMessages();
                     $data['status'] = 400;
                    
                }
            }
            
            $data['flag']=$flag;
            $data['message']=$message;
            
            return new JsonModel($data);
            
        
    }
    
    
    public function showAction(){
        $request = $this->getRequest();
        $em = $this->getEntityManager();
        $data = array('content'=>'','flag'=>false);
        $request = $this->getRequest();
        $employee =  $em->getRepository('\Application\Entity\Employee')->getInfo($this->params()->fromRoute('id'));
           
        if($request->isXmlHttpRequest()) {
           
            $viewModelo  = new ViewModel(array('employee' => $employee));
            $viewModelo->setTemplate('application/index/partial/view.phtml');
            $rendered = $this->getServiceLocator()->get('ViewRenderer');
            $data['content']  = $rendered->render($viewModelo);
            $data['flag'] = true;
        } else {
            $data['flag'] = true;
            $data['content']  = $employee;
        }
        
        return new JsonModel($data);
        
        
    }
}
