<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inputs
 *
 * @ORM\Table(name="inputs", indexes={@ORM\Index(name="id_employee", columns={"id_employee"})})
 * @ORM\Entity
 */
class Inputs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=false)
     */
    private $birthdate;

    /**
     * @var float
     *
     * @ORM\Column(name="anual_input", type="float", precision=10, scale=0, nullable=false)
     */
    private $anualInput;

    /**
     * @var \Application\Entity\Employee
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Employee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_employee", referencedColumnName="id")
     * })
     */
    private $idEmployee;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return Inputs
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime 
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set anualInput
     *
     * @param float $anualInput
     * @return Inputs
     */
    public function setAnualInput($anualInput)
    {
        $this->anualInput = $anualInput;

        return $this;
    }

    /**
     * Get anualInput
     *
     * @return float 
     */
    public function getAnualInput()
    {
        return $this->anualInput;
    }

    /**
     * Set idEmployee
     *
     * @param \Application\Entity\Employee $idEmployee
     * @return Inputs
     */
    public function setIdEmployee(\Application\Entity\Employee $idEmployee = null)
    {
        $this->idEmployee = $idEmployee;

        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return \Application\Entity\Employee 
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }
}
