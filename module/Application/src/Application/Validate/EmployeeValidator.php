<?php

namespace Application\Validate; 

use Zend\InputFilter\Factory as InputFactory; 
use Zend\InputFilter\InputFilter; 
use Zend\InputFilter\InputFilterAwareInterface; 
use Zend\InputFilter\InputFilterInterface; 

class EmployeeValidator implements InputFilterAwareInterface{
    
    protected $inputFilter; 
    
    public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used"); 
    }
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => "Introduzca un nombre"
                            )
                        )
                    ),
                ),
            )));//nombre
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'firstname',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => "Introduzca apellido paterno"
                            )
                        )
                    ),
                ),
            )));//firstname
            
             $inputFilter->add($factory->createInput(array(
                'name'     => 'lastname',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => "Introduzca apellido materno"
                            )
                        )
                    ),
                ),
            )));//firstname
            
            $inputFilter->add($factory->createInput(array(
                'name'     => 'birthdate',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => "Introduzca su fecha de nacimiento"
                            )
                        )
                    ),
                    array(
                        'name' => 'Date',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\Date::FALSEFORMAT =>'El fomrato de fecha no es el correcto',
                            \Zend\Validator\Date::INVALID_DATE =>'La fecha introducida es invalida el formato es dd/mm/YYYY'
                            ),
                            'format'=>'d/m/Y'
                        )
                    ),
                ),
            )));//Fecha de nacimiento
            
             $inputFilter->add($factory->createInput(array(
                'name'     => 'inputYears',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                   array(
                        'name' => 'not_empty',
                        'options' => array(
                            'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => "Introduzca datos al campo"
                            )
                        )
                    ),
                ),
            )));//firstname
            
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
}