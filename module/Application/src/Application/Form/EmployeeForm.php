<?php
/**
 * Formulario para la contratacion 
 * @version 1.0
 */
namespace Application\Form;
use Zend\Form\Form;


class EmployeeForm extends Form{
    
    public function __construct() {
        parent::__construct('recruiterForm');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form');
        
        
       $this->add(array( 
            'name' => 'name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'class' => 'form-control',  
                'required' => 'required',
            ), 
            'options' => array(
                'label' => 'Nombre', 
            ), 
        ));//**Nombre*
        
        $this->add(array( 
            'name' => 'firstname', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'class' => 'form-control',  
                'required' => 'required',
            ), 
            'options' => array(
                'label' => 'Apellido paterno', 
            ), 
        ));//**Nombre*
        
         $this->add(array( 
            'name' => 'lastname', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'class' => 'form-control',  
                'required' => 'required',
            ), 
            'options' => array(
                'label' => 'Apellido materno', 
            ), 
        ));//**Nombre*
         
         $this->add(array( 
            'name' => 'birthdate', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'class' => 'form-control',  
                'required' => 'required',
            ), 
            'options' => array(
                'label' => 'Fecha de nacimiento', 
            ), 
        ));//**Nombre*
        
         
         $this->add(array( 
            'name' => 'inputYears', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'class' => 'form-control',  
                'required' => 'required',
            ), 
            'options' => array(
                'label' => 'Ingresos anuales', 
            ), 
        ));//**Nombre*
         
        $this->add(array( 
            'name' => 'csrf', 
            'type' => 'Zend\Form\Element\Csrf', 
        ));//Security Field
        
        
    }
}
    
   
