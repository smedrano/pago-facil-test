<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class EmployeeRepository extends EntityRepository {
    
    /**
     * Se obtienen todos los empleados
     * @return boolean
     */
    public function getAll()
    {
        $qb = $this->_em->createQueryBuilder(); 
        $qb->select('e')
                ->from('Application\Entity\Employee','e');
                
        $employee =  $qb->getQuery()->execute();    
        
        return $employee;
    }
    
    public function getInfo($id){
        $qb = $this->_em->createQueryBuilder(); 
        $qb->select('e,i')
                ->from('Application\Entity\Employee','e')
                ->innerJoin('Application\Entity\Inputs', 'i','WITH','i.idEmployee=e.id')
                ->where( 'e.id = ?1')
                ->setParameter(1,$id)
                ->setMaxResults(1);
                
        $employee =  $qb->getQuery()->execute(null,  \Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR)[0]; 
        return $employee;
    }
}
